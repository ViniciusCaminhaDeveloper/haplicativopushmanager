FROM openjdk:8
ADD Consumer/target/hapvida-queue-0.0.1-SNAPSHOT.jar hapvida-queue-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar", "hapvida-queue-0.0.1-SNAPSHOT.jar"]