package com.hapvida.queue;

import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/queue")
public class BeneficiarioListenerComponent {
	
	HttpPost post = new HttpPost("http://jakarata.apache.org/");

    @Autowired private JmsTemplate jmsTemplate;

    @PostMapping("/agendaPushQueue")
    public String teste(@RequestBody String numeroBeneficiario) {
    	jmsTemplate.convertAndSend("queue.pushBeneficiario", numeroBeneficiario);
    	
    	System.out.println("Chegou no Producer, adicionando  :" + numeroBeneficiario);
	  
		return numeroBeneficiario;
    }



}
